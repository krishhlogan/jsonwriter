"""jsonwriter URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from . import views
urlpatterns = [
    path('sweet/writer/',views.writer_sweetcorn ,name='writer'),
    path('sweet/',views.reader_sweetcorn,name='reader'),
    path('sweet/delete/',views.delete_sweetcorn,name="delete"),
    path('madisson/writer/',views.writer_madisson ,name='writer'),
    path('madisson/',views.reader_madisson,name='reader'),
    path('madisson/delete/',views.delete_madisson,name="delete")

]
