from django.shortcuts import render
import json
from django.http import JsonResponse,HttpResponse
import os
from django.views.decorators.csrf import csrf_exempt



# Create your views here.
@csrf_exempt
def writer_sweetcorn(request):
    print("krish")
    if request.method=="POST":
        print("POST")
        data=json.loads(request.body)
        data=data['data']
        print(data)
        with open("sweetcorn.json","r") as f:
            actual_data=json.load(f)
        actual_data.append(data)
        with open('sweetcorn.json', 'w') as outfile:
            json.dump(actual_data, outfile)
        return JsonResponse(data=actual_data,safe=False)
    else:
        print("get")
        print(request.body)
        return JsonResponse(data={'status':'NO'},safe=False)

@csrf_exempt
def reader_sweetcorn(request):
    print("Path is",os.getcwd())
    with open(os.getcwd()+"/sweetcorn.json","r") as f:
        data=json.load(f)
    print("data is",data)
    return JsonResponse(data=data,safe=False)


@csrf_exempt
def delete_sweetcorn(request):
    if request.method=="POST":
        print("POST")
        data=json.loads(request.body)
        data=data['data']
        print(data)
        with open("sweetcorn.json","r") as f:
            actual_data = json.load(f)
        try:
            del actual_data[int(data)]
        except Exception as e:
            return JsonResponse(data={"error":str(e)},safe=False)

        with open('sweetcorn.json', 'w') as outfile:
            json.dump(actual_data, outfile)

        return JsonResponse(data=actual_data,safe=False)




# Create your views here.
@csrf_exempt
def writer_madisson(request):
    print("krish")
    if request.method=="POST":
        print("POST")
        data=json.loads(request.body)
        data=data['data']
        print(data)
        with open("madisson.json","r") as f:
            actual_data=json.load(f)
        actual_data.append(data)
        with open('madisson.json', 'w') as outfile:
            json.dump(actual_data, outfile)
        return JsonResponse(data=actual_data,safe=False)
    else:
        print("get")
        print(request.body)
        return JsonResponse(data={'status':'NO'},safe=False)

@csrf_exempt
def reader_madisson(request):
    print("Path is",os.getcwd())
    with open(os.getcwd()+"/madisson.json","r") as f:
        data=json.load(f)
    print("data is",data)
    return JsonResponse(data=data,safe=False)


@csrf_exempt
def delete_madisson(request):
    if request.method=="POST":
        print("POST")
        data=json.loads(request.body)
        data=data['data']
        print(data)
        with open("madisson.json","r") as f:
            actual_data = json.load(f)
        try:
            del actual_data[int(data)]
        except Exception as e:
            return JsonResponse(data={"error":str(e)},safe=False)

        with open('madisson.json', 'w') as outfile:
            json.dump(actual_data, outfile)

        return JsonResponse(data=actual_data,safe=False)